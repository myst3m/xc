(ns xc.core
  (:gen-class)
  (:import [sun.misc Signal]
           [sun.misc SignalHandler]
           [java.lang ProcessHandle ProcessBuilder Runtime]
           [java.util.zip ZipEntry ZipInputStream]
           [java.io FileInputStream]
           [clojure.lang PersistentQueue])
  (:require [sci.core :as sci]
            [sci.impl.interpreter :as i]
            [sci.impl.vars :as vars]
            [sci.impl.io :as sio]
            [sci.impl.opts :as sopts]
            [sci.impl.namespaces :as sn]
            [clojure.java.io :as io]
            [clojure.java.shell]
            [clojure.string :as str]
            [clojure.edn :as edn]
            [clojure.xml]
            [clojure.zip]
            [org.httpkit.client]
            [org.httpkit.server]
            [silvur.datetime :refer :all]
            [silvur.util]
            [silvur.http]
            [fipp.clojure :as fipp]
            [clojure.tools.cli :refer (parse-opts)]
            [clojure.core.async :as async]
            [clojure.java.shell :as shell]
            [clojure.xml :as xml])
  ;; (:import [org.jline.terminal TerminalBuilder]
  ;;          [org.jline.reader LineReaderBuilder])
  )

(set! *warn-on-reflection* true)

(def xc-version "0.7.5")

(def default-repl-command ["nohup" "clojure" "-Sdeps" "{:deps {nrepl {:mvn/version \"0.8.3\"} cider/cider-nrepl {:mvn/version \"0.25.4\"}}}" "-m" "nrepl.cmdline" ;;"-t" "nrepl.transport/tty"
                           ])
(defn exists? [^java.io.File f]
  (.exists f))

(defn nrepl-pid []
  (when (.exists (io/as-file ".nrepl-pid"))
    (let [pid (slurp ".nrepl-pid")
          {:keys [exit]} (shell/sh "kill" "-0" pid)]
      (if (= 0 exit)
        pid
        (try
          (io/delete-file ".nrepl-pid")
          nil
          (catch Exception e))))))

(defn nrepl-start [& cmd-with-args]
  (let [cmd (ProcessBuilder.
             ^"[Ljava.lang.String;" (into-array (if (seq cmd-with-args)
                                                  cmd-with-args
                                                  default-repl-command)))]
    (or (nrepl-pid)
        (let [proc (.start cmd)
              _ (Thread/sleep 500)
              proc-handle (-> proc (.children) (.toArray) (first))]
          
          (when proc-handle
            (let [pid (.pid ^ProcessHandle proc-handle)]
              (-> (.onExit ^ProcessHandle proc-handle)
                  (.thenAccept
                   (proxy [java.util.function.Consumer] []
                     (accept [^ProcessHandle ph]
                       (io/delete-file (str ".nrepl-" (.pid ph)))))))
              (spit (str ".nrepl-pid") pid)
              pid))))))



(defn nrepl-kill [& pids]
  (doseq [pid (->> (io/as-file ".")
                   (file-seq)
                   (filter #(re-find #".nrepl-[1-9][0-9]*" (str %)))
                   (map slurp))]
    (shell/sh "kill" pid)))

(defn compile* [port & name-spaces]
  ;; (with-open [conn (nrepl/connect :port port)]
  ;;   (-> (nrepl/client conn 1000)
  ;;       (nrepl/message {:op "eval" :code "(compile '" (first name-spaces) ")"})
  ;;       (nrepl/response-values)))
  )


(def input-var (sci/new-dynamic-var '*input* nil))
(def command-line-args-var (sci/new-dynamic-var '*command-line-args* nil))

(def ^java.util.concurrent.Executor executor @#'async/thread-macro-executor)

(defn thread-call
  "Executes f in another thread, returning immediately to the calling
  thread. Returns a channel which will receive the result of calling
  f when completed, then close."
  [f]
  (let [c (async/chan 1)]
    (let [binds (vars/get-thread-binding-frame)]
      (.execute executor
                (fn []
                  (vars/reset-thread-binding-frame binds)
                  (try
                    (let [ret (f)]
                      (when-not (nil? ret)
                        (async/>!! c ret)))
                    (finally
                      (async/close! c))))))
    c))

(defn thread
  [_ _ & body]
  `(~'clojure.core.async/thread-call (fn [] ~@body)))

(defn go-loop
  [_ _ bindings & body]
  (list 'clojure.core.async/thread (list* 'loop bindings body)))


(def opts {:load-fn (fn [{:keys [namespace]}]
                      {:file   (str namespace ".clj")
                       :source (slurp (str namespace ".clj"))})
           :namespaces
           {'clojure.pprint     {'pprint #'fipp.clojure/pprint}
            'clojure.core       {'slurp               #'clojure.core/slurp
                                 'spit                #'clojure.core/spit
                                 '*command-line-args* command-line-args-var}
            'clojure.core.async {'go          (with-meta thread {:sci/macro true})
                                 'go-loop     #'go-loop
                                 'thread      (with-meta thread {:sci/macro true})
                                 'thread-call #'thread-call
                                 'chan        #'clojure.core.async/chan
                                 '<!          #'clojure.core.async/<!!
                                 '<!!         #'clojure.core.async/<!!
                                 '>!          #'clojure.core.async/>!!
                                 '>!!         #'clojure.core.async/>!!                                             
                                 'timeout     #'clojure.core.async/timeout
                                 'put!        #'clojure.core.async/put!
                                 'poll!       #'clojure.core.async/poll!}
            'clojure.java.shell {'sh #'clojure.java.shell/sh}
            'clojure.java.io    {'file               #'clojure.java.io/file,
                                 'make-output-stream #'clojure.java.io/make-output-stream,
                                 'make-parents       #'clojure.java.io/make-parents,
                                 'delete-file        #'clojure.java.io/delete-file,
                                 'input-stream       #'clojure.java.io/input-stream,
                                 'make-writer        #'clojure.java.io/make-writer,
                                 'as-relative-path   #'clojure.java.io/as-relative-path,
                                 'copy               #'clojure.java.io/copy,
                                 'as-file            #'clojure.java.io/as-file,
                                 'output-stream      #'clojure.java.io/output-stream,
                                 'make-reader        #'clojure.java.io/make-reader,
                                 'make-input-stream  #'clojure.java.io/make-input-stream,
                                 'resource           #'clojure.java.io/resource,
                                 'writer             #'clojure.java.io/writer,
                                 'as-url             #'clojure.java.io/as-url,
                                 'reader             #'clojure.java.io/reader}
            'clojure.string     {'blank?               #'clojure.string/blank?,
                                 'capitalize           #'clojure.string/capitalize,
                                 'ends-with?           #'clojure.string/ends-with?,
                                 'escape               #'clojure.string/escape,
                                 'includes?            #'clojure.string/includes?,
                                 'index-of             #'clojure.string/index-of,
                                 'join                 #'clojure.string/join,
                                 'last-index-of        #'clojure.string/last-index-of,
                                 'lower-case           #'clojure.string/lower-case,
                                 're-quote-replacement #'clojure.string/re-quote-replacement,
                                 'replace              #'clojure.string/replace,
                                 'replace-first        #'clojure.string/replace-first,
                                 'reverse              #'clojure.string/reverse,
                                 'split                #'clojure.string/split,
                                 'split-lines          #'clojure.string/split-lines,
                                 'starts-with?         #'clojure.string/starts-with?,
                                 'trim                 #'clojure.string/trim,
                                 'trim-newline         #'clojure.string/trim-newline,
                                 'triml                #'clojure.string/triml,
                                 'trimr                #'clojure.string/trimr,
                                 'upper-case           #'clojure.string/upper-case}
            'clojure.set        {'difference   #'clojure.set/difference,
                                 'index        #'clojure.set/index,
                                 'intersection #'clojure.set/intersection,
                                 'join         #'clojure.set/join,
                                 'map-invert   #'clojure.set/map-invert,
                                 'project      #'clojure.set/project,
                                 'rename       #'clojure.set/rename,
                                 'rename-keys  #'clojure.set/rename-keys,
                                 'select       #'clojure.set/select,
                                 'subset?      #'clojure.set/subset?,
                                 'superset?    #'clojure.set/superset?,
                                 'union        #'clojure.set/union}
            'clojure.xml        {'attrs           #'clojure.xml/attrs,
                                 'content         #'clojure.xml/content,
                                 'content-handler #'clojure.xml/content-handler,
                                 'element         #'clojure.xml/element,
                                 'emit            #'clojure.xml/emit,
                                 'emit-element    #'clojure.xml/emit-element,
                                 'parse           #'clojure.xml/parse,
                                 'startparse-sax  #'clojure.xml/startparse-sax,
                                 'tag             #'clojure.xml/tag}
            'org.httpkit.client {'options         #'org.httpkit.client/options,
                                 'put             #'org.httpkit.client/put,
                                 'query-string    #'org.httpkit.client/query-string,
                                 'proppatch       #'org.httpkit.client/proppatch,
                                 'request         #'org.httpkit.client/request,
                                 'get             #'org.httpkit.client/get,
                                 'copy            #'org.httpkit.client/copy,
                                 'patch           #'org.httpkit.client/patch,
                                 'make-ssl-engine #'org.httpkit.client/make-ssl-engine,
                                 'move            #'org.httpkit.client/move,
                                 'delete          #'org.httpkit.client/delete,
                                 'make-client     #'org.httpkit.client/make-client,
                                 'url-encode      #'org.httpkit.client/url-encode,
                                 'head            #'org.httpkit.client/head,
                                 'propfind        #'org.httpkit.client/propfind,
                                 'post            #'org.httpkit.client/post,
                                 'acl             #'org.httpkit.client/acl}
            'clojure.zip        {'append-child #'clojure.zip/append-child,
                                 'branch?      #'clojure.zip/branch?,
                                 'children     #'clojure.zip/children,
                                 'down         #'clojure.zip/down,
                                 'edit         #'clojure.zip/edit,
                                 'end?         #'clojure.zip/end?,
                                 'insert-child #'clojure.zip/insert-child,
                                 'insert-left  #'clojure.zip/insert-left,
                                 'insert-right #'clojure.zip/insert-right,
                                 'left         #'clojure.zip/left,
                                 'leftmost     #'clojure.zip/leftmost,
                                 'lefts        #'clojure.zip/lefts,
                                 'make-node    #'clojure.zip/make-node,
                                 'next         #'clojure.zip/next,
                                 'node         #'clojure.zip/node,
                                 'path         #'clojure.zip/path,
                                 'prev         #'clojure.zip/prev,
                                 'remove       #'clojure.zip/remove,
                                 'replace      #'clojure.zip/replace,
                                 'right        #'clojure.zip/right,
                                 'rightmost    #'clojure.zip/rightmost,
                                 'rights       #'clojure.zip/rights,
                                 'root         #'clojure.zip/root,
                                 'seq-zip      #'clojure.zip/seq-zip,
                                 'up           #'clojure.zip/up,
                                 'vector-zip   #'clojure.zip/vector-zip,
                                 'xml-zip      #'clojure.zip/xml-zip,
                                 'zipper       #'clojure.zip/zipper}
            'org.httpkit.server {'run-server   #'org.httpkit.server/run-server,
                                 'with-channel #'org.httpkit.server/with-channel}
            'silvur.datetime
            {'+time                  #'silvur.datetime/+time,
             'JST                    #'silvur.datetime/JST,
             'NYC                    #'silvur.datetime/NYC,
             'UTC                    #'silvur.datetime/UTC,
             'adjust                 #'silvur.datetime/adjust,
             ;; 'calendar<-             #'silvur.datetime/calendar<-,
             ;; 'date<-                 #'silvur.datetime/date<-,
             'datetime               #'silvur.datetime/datetime,
             'datetime*              #'silvur.datetime/datetime*,
             'day                    #'silvur.datetime/day,
             'first-date-of-week     #'silvur.datetime/first-date-of-week,
             'hour                   #'silvur.datetime/hour,
             'inst<                  #'silvur.datetime/inst<,
             'minute                 #'silvur.datetime/minute,
             'minutes-of-week        #'silvur.datetime/minutes-of-week,
             'set-default-precision! #'silvur.datetime/set-default-precision!,
             'set-default-tz!        #'silvur.datetime/set-default-tz!,
             ;; 'this-month             #'silvur.datetime/this-month,
             ;; 'this-month*            #'silvur.datetime/this-month*,
             ;; 'this-year              #'silvur.datetime/this-year,
             ;; 'this-year*             #'silvur.datetime/this-year*,
             ;; 'time-period-seq        #'silvur.datetime/time-period-seq,
             'time-seq               #'silvur.datetime/time-seq,
             'today                  #'silvur.datetime/today,
             ;; 'today*                 #'silvur.datetime/today*,
             ;; 'tz->                   #'silvur.datetime/tz->,
             'vec<                   #'silvur.datetime/vec<,
             'yesterday              #'silvur.datetime/yesterday,
             ;; 'yesterday*             #'silvur.datetime/yesterday*
             }
            'silvur.http        {'get           #'silvur.http/get
                                 'post          #'silvur.http/post
                                 'sni-configure silvur.http/sni-configure}
            'silvur.util        {'str->int  #'silvur.util/str->int
                                 'json->edn #'silvur.util/json->edn
                                 'edn->json #'silvur.util/edn->json}
            'fipp.engine        {'pprint-document #'fipp.engine/pprint-document}
            'xc.file            {'exists? #'exists?}
            'xc.core            {'nrepl-start #'nrepl-start
                                 'nrepl-kill  #'nrepl-kill
                                 'compile     #'compile*}
            'user               {'*input* input-var}}})


(def cli-opts
  [["-r" "--require <FILE>" "Require libraries"]
   [nil "--edn" "Bind *input* to lazy seq of EDN values"]
   [nil "--force" "Run command forcely"]
   ["-h" "--help" "This help"]])

(def NL "\n")

(defn print-usage [summary]
  (println "Usage:" NL NL
           "xc [command] [options]" NL NL)
  (println "Commands:")
  (println " - new <project name>")                            
  (println " - template <project name> [web]")
  (println " - server")                          
  (println " - deps")
  (println " - ancient" NL)                                                    
  (println "Options:")
  (println summary)
  (println))

(defn edn-seq [in]
  (let [v (edn/read {:eof ::EOF} in)]
    (when-not (identical? ::EOF v)
      (cons v (lazy-seq (edn-seq in))))))

(defn shell-seq [in]
  (line-seq (java.io.BufferedReader. in)))

(defn make-init-files [project-name project-root-dir]
  (let [L ""
        readme (str (or project-root-dir ".") "/README.md")]
    (when-not (.exists (io/file readme))
      (spit readme
            (str/join "\n" [(str "# " project-name "")
                            L
                            (str "This software is for your better life.")
                            L
                            (str "## Install")
                            L
                            (str "## Usage")
                            L])))))

(defn- init* [{:keys [options arguments summary] :as opts}]
  (let [[_ project-name] arguments
        project-root (when project-name (str project-name "/"))
        env-root-dir (str  project-root ".xc/.")
        src-dir (str project-root "src/.")]
    (try
      (let [ms (edn/read-string (:body @(silvur.http/get "https://gitlab.com/myst3m/xc/-/raw/master/resources/modules.edn" {})))]
        (io/make-parents env-root-dir)
        (io/make-parents src-dir)
        (make-init-files project-name project-root)
        (doseq [m ms]
          (try
            (let [{:keys [status body]} @(silvur.http/get (str "https://gitlab.com/myst3m/xc/-/raw/master/resources/" m ".clj") {})]
              (if (= 200 status)
                (spit (str env-root-dir "/" m ".clj") body)
                (println "Failed to download module" m)))
            (catch Exception e (println "Failed to download module" m)))))
      (catch Exception e (println "Failed to download module list")))))


(defn- new* [{:keys [options arguments summary] :as opts}]
  (let [[_ project-name] arguments
        core-file (io/file project-name "src" project-name "core.clj" )]
    (if (seq arguments)
      (if (or (not (.exists (io/as-file project-name))) (:force options))
        (do (io/make-parents core-file)
            ;; (spit core-file
            ;;       (str "(ns " project-name ".core)\n\n"
            ;;            "(defn -main [& args]\n"
            ;;            "  (println " (pr-str "Hello" project-name)"))\n"))
            (init* opts))
        (println (str "Project " project-name " already exists. Add --force to generate forcely")))
      (print-usage summary))))


(def pipe-state (volatile! nil))

(defn pipe-signal-received? []
  (identical? :PIPE @pipe-state))

(defn handle-pipe! []
  (Signal/handle
   (Signal. "PIPE")
   (reify SignalHandler
     (handle [_ _]
       (vreset! pipe-state :PIPE)))))


(defn interpret [ctx s]
  (try
    (let [cmd-ctx (cond-> {:cmds s :shell? false :background? false}
                    (re-find #"^[ \t]*!.+" s) (-> (update :cmds #(subs % 1))
                                                  (assoc :shell? true))
                    (re-find #"&$" s) (-> (update :cmds #(str/join "" (butlast %)))
                                          (assoc :background? true)))]
      (if (:shell? cmd-ctx)
        (let [cmd-line (->> (str/split (:cmds cmd-ctx) #"[ \t]+")
                            (map str/trim)
                            (filter (comp not empty?)))]
          (if (:background? cmd-ctx)
            (clojure.core.async/go (apply clojure.java.shell/sh cmd-line))
            (let [{:keys [out err]} (apply clojure.java.shell/sh cmd-line)]
              (println out err))))
        (println (i/eval-string*  ctx s))))
    (flush)
    (catch Exception e (println e))))



(defn pom-xml [^String jar-file]
  (with-open [zis (ZipInputStream. (FileInputStream. jar-file))]
    (loop []
      (if-let [ze (.getNextEntry zis)]
        (if (re-find #"pom.xml" (.getName ze))
          (let [data (reduce (fn [r _]
                               (let [buf (byte-array 10240)
                                     size (.read zis buf)]
                                 (if (= -1 size)
                                   (reduced r)
                                   (concat r (take size buf)))))
                             []
                             (repeat 0))]
            
            (->> (byte-array data)
                 (io/input-stream)
                 (xml/parse)
                 (xml-seq)
                 (filter #(= :dependency (:tag %)))))
          (recur))))))

(defn -main
  [& args]
  (let [{:keys [options arguments summary] :as given-opts} (parse-opts args cli-opts)
        [subcmd] arguments]
    (handle-pipe!)
    ;; Command dispatch
    (cond
      (:help options) (print-usage summary)
      (= subcmd "init") (init* given-opts)
      (= subcmd "new") (new* given-opts)
      (= subcmd "server") (println (nrepl-start))
      :else (sci/binding [sci/in *in* 
                          sci/out *out*
                          command-line-args-var args
                          input-var (delay
                                      (if (:edn options)
                                        (edn-seq *in*)
                                        (shell-seq *in*)))]
              (vars/with-bindings
                ;; Set namespace
                (when-not @vars/current-ns
                  {vars/current-ns vars/user-ns})

                ;; Load fundamental libraries
                (let [ctx (doto (sopts/init opts)
                            (i/eval-string* "(require '[clojure.string :as str])")
                            (i/eval-string* "(require '[clojure.java.shell :as shell])")
                            (i/eval-string* "(require '[clojure.core.async :as async])")          
                            (i/eval-string* "(require '[clojure.java.io :as io])")
                            (i/eval-string* "(require '[clojure.pprint :as pp :refer (pprint)])")
                            (i/eval-string* "(require '[clojure.xml :as xml])")
                            (i/eval-string* "(require '[clojure.zip :as zip])")
                            (i/eval-string* "(require '[org.httpkit.client])")
                            (i/eval-string* "(require '[silvur.http :as http])")
                            (i/eval-string* "(require '[silvur.datetime :refer :all])")
                            (i/eval-string* "(require '[silvur.util :as util])")
                            (i/eval-string* "(require '[xc.core :refer :all])"))]


                  ;; Load built-in files
                  (doseq [m (->> (file-seq (io/file ".xc"))
                                 (filter #(re-find #".clj$" (str %))))]
                    (i/eval-string* ctx (slurp m)))

                  ;; Back to user name space
                  (i/eval-string* ctx (str "(in-ns '" (str vars/user-ns) ")"))
                  
                  ;; Load specified file by the comand line option
                  (when-let [modules (:require options)] 
                    (doseq [module-file (str/split modules #",")]
                      (i/eval-string* ctx (slurp module-file))))

                  
                  (if-let [file-or-str (first arguments)]
                    ;; Eval function by command line with parentheses
                    (let [s (cond
                              (.exists (io/as-file file-or-str)) (slurp (io/file file-or-str))
                              (re-find #"^\(" file-or-str) file-or-str
                              :else (str "(xc.command/" file-or-str " " (pr-str given-opts) ")"))]
                      (i/eval-string* ctx s)
                      nil)
                    ;; REPL
                    (do
                      (println "xc " xc-version)
                      (loop [ctx ctx]
                        (print "> ")
                        (flush)
                        (let [s (read-line)]
                          (if-not s
                            (do (println "Quit")
                                (System/exit 0))
                            (do (interpret ctx s)
                                (recur ctx)))))))))))
    (shutdown-agents)))





