(ns xc.command
  (:require [clojure.pprint :as pp]
            [silvur.http :as http]
            [silvur.util :as util]
            [clojure.string :as str]
            [clojure.java.io :as io]
            [clojure.java.shell :as shell]
            [clojure.core.async :as async]
            [fipp.engine :refer (pprint-document)]))

(defn template-deps [project-name]
  {:paths ["src" "resources"]
   :deps {'org.clojure/clojure {:mvn/version "1.11.1"}
          'org.clojure/tools.cli {:mvn/version "1.0.206"}
          'io.gitlab.myst3m/silvur {:mvn/version "2.3.6"}}
   :aliases {:test {:extra-paths ["test"]
                    :extra-deps {'org.clojure/test.check {:mvn/version "0.10.0"}}}
             :runner
             {:extra-deps {'com.cognitect/test-runner
                           {:git/url "https://github.com/cognitect-labs/test-runner"
                            :sha "f7ef16dc3b8332b0d77bc0274578ad5270fbfedd"}}
              :main-opts ["-m" "cognitect.test-runner"
                          "-d" "test"]}

             :build {:main-opts ["-m" "silvur.build"]}
             :deploy {:main-opts ["-m" "silvur.build" "deploy"]}
             :uberjar {:exec-fn 'silvur.build/exec-uberjar
                       :exec-args {:artifact (str "io.gitlab.myst3m/" project-name)
                                   :version "0.0.1"
                                   :aot true
                                   :main (str project-name ".core")}}
             :javac {:main-opts ["-m" "silvur.build" "javac"]}}})

;; Commands
(defn- deps-file [project-name template-name]
  (let [deps (template-deps project-name)]
    (condp = (keyword template-name)
      :web (-> deps
               (assoc-in  [:deps 'http-kit/http-kit] {:mvn/version "2.5.1"})
               (assoc-in  [:deps 'metosin/reitit] {:mvn/version "0.5.16"})
               (assoc-in  [:deps 'mount/mount] {:mvn/version "0.1.16"})
               (assoc-in  [:deps 'hiccup/hiccup] {:mvn/version "1.0.5"})
               (assoc-in  [:deps 'org.clojure/spec.alpha] {:mvn/version "0.3.214"}))
      :mule (-> deps
                (assoc-in  [:deps 'io.gitlab.myst3m/mulify] {:mvn/version "0.3.0"})
                (assoc-in [:mvn/repos "mule-release"] {:url "https://repository.mulesoft.org/releases/"})
                (assoc-in [:mvn/repos "clojars"] {:url "https://clojars.org/repo"})
                (assoc-in  [:deps 'org.clojure/spec.alpha] {:mvn/version "0.3.214"})
                (assoc-in [:mvn/repos "jitpack"] {:url "https://jitpack.io"}))
      deps)))


(defn- main-functions [wrt]
  (pp/pprint '(def cli-options [["-h" "--help" "This Help"]]) {:writer wrt})
  (pprint-document [:pass] {:writer wrt})
  (pp/pprint '(defn usage [summary]
                (println)
                (println "Usage:")
                (println)
                (println summary)
                (println))
             {:writer wrt})
  (pprint-document [:pass] {:writer wrt})
  (pp/pprint '(defn -main [& args]
                (let [{:keys [options arguments summary errors]} (parse-opts args cli-options)]
                  (if (:help options)
                    (usage summary)
                    (do
                      (println "Hello World")))))
             {:writer wrt}))


(defn- web-core-file [project-name]
  ;; (when (.exists (io/file f))
  ;;   (io/copy (io/file f) (io/file (str f ".bak")))
  ;;   (io/delete-file (io/file f)))
  (let [underscored-project (str/replace (or project-name "project") #"-" "_")
        wrt (io/writer (io/file "src" underscored-project "core.clj") :append true)]
    (pp/pprint (list 'ns (symbol (str project-name ".core"))
                     (list ':gen-class)
                     (list ':require
                           '[clojure.tools.cli :refer (parse-opts)]
                           '[reitit.http :as rh]
                           '[reitit.ring :as ring]
                           '[silvur.datetime :refer (datetime datetime*)]
                           '[silvur.util :refer (json->edn edn->json)]                           
                           '[reitit.interceptor.sieppari :as sieppari]
                           '[reitit.coercion.spec]
                           '[reitit.swagger :as swagger]
                           '[clojure.spec.alpha :as s]
                           '[hiccup.core :refer (html)]
                           '[hiccup.page :as page]
                           '[muuntaja.interceptor :as mi]
                           '[muuntaja.core :as m]
                           '[mount.core :refer [defstate] :as mount]
                           '[org.httpkit.server :refer (run-server)]))
               {:writer wrt})
    (pprint-document [:pass] {:writer wrt})
    (pp/pprint '(defn home []
                  (html [:html
                         [:head
                          [:meta {:charset "utf-8"}]
                          [:title "Home"]
                          [:style (str "body {background-color: #131722; color: #8F8F8F}; ")]]
                         [:body
                          [:div "Hello World"]
                          (page/include-js "/js/main.js")]]))
               {:writer wrt})
    (pprint-document [:pass] {:writer wrt})

    (pp/pprint (list 's/def (symbol "::user-id") 'int?) {:writer wrt})
    (pp/pprint (list 's/def (symbol "::company") 'string?) {:writer wrt})
    (pp/pprint (list 's/def (symbol "::query-params") (list 's/keys :opt-un [(symbol "::q")])) {:writer wrt})
    (pp/pprint (list 's/def (symbol "::path-params") (list 's/keys :opt-un [(symbol "::company") (symbol "::user-id")])) {:writer wrt})
    (pp/pprint (list 's/def (symbol "::common-headers") (list 's/keys :opt-un [(symbol "::Authorization") (symbol "::Content-Type")])) {:writer wrt})
    (pprint-document [:pass] {:writer wrt})
    (doseq [item [(list 'def 'app-routes [""
                                          ["/swagger.json" {:no-doc true
                                                            :swagger {:info {:title "my-api"
                                                                             :version "v1"
                                                                             :produces [:application/json]}}
                                                            :handler '(swagger/create-swagger-handler)
                                                            :interceptors ['(mi/format-interceptor)]}]
                                          ["/api/:company/users/:user-id" {:get {:handler '(fn [req] {:status 200 })
                                                                                 :coercion 'reitit.coercion.spec/coercion
                                                                                 :parameters {:header (symbol "::common-headers")
                                                                                              :query (symbol "::query-params")
                                                                                              :path (symbol "::path-params")}}}]])
                  '(def app
                     (rh/ring-handler
                      (rh/router app-routes )
                      (ring/routes
                       (ring/create-resource-handler {:path "/"})
                       (ring/create-default-handler))
                      {:executor sieppari/executor}))

                   

                   '(defn start []
                     (run-server #'app (merge {:port 8082} (mount/args) )))
                   '(defn stop [server]
                     (server))
                   
                   '(defstate server :start (start) :stop (stop server))]]
      (pp/pprint item {:writer wrt})
      (pprint-document [:pass] {:writer wrt}))
    (main-functions wrt)))

(defn- mule-core-file [project-name]
  (let [underscored-project (str/replace (or project-name "project") #"-" "_")
        wrt (io/writer (io/file "src" underscored-project "core.clj") :append true)
        default-main-xml (str project-name ".xml")]
    (pp/pprint (list 'ns (symbol (str project-name ".core"))
                     '(:gen-class)
                     '(:refer-clojure :exclude [error-handler])
                     '(:require
                       [clojure.string :as str]
                       [clojure.java.io :as io]
                       [clojure.tools.cli :refer (parse-opts)]
                       [silvur.datetime :refer (datetime datetime*)]
                       [silvur.util :refer (json->edn edn->json)]
                       [mulify
                        [core :as c :refer [defapi]]
                        [http :as http]
                        [os :as os]
                        [vm :as vm]
                        [ee :as ee]
                        [db :as db]
                        [tls :as tls]
                        [dataweave :as dw]
                        [batch :as batch]
                        [apikit :as ak]
                        [apikit :as ak-odata]
                        [jms :as jms]
                        [wsc :as wsc]
                        [anypoint-mq :as amq]
                        [api-gateway :as gw]
                        [generic]
                        [file :as f]]
                       [mulify.utils :as utils]))
               
               
               {:writer wrt})
    
    (pp/pprint '(def mule-app (mulify.core/mule
                               {:xmlns:core          "http://www.mulesoft.org/schema/mule/core",
                                :xmlns:http          "http://www.mulesoft.org/schema/mule/http",
                                :xmlns:file          "http://www.mulesoft.org/schema/mule/file",
                                :xmlns:documentation "http://www.mulesoft.org/schema/mule/documentation",
                                :xmlns:ee            "http://www.mulesoft.org/schema/mule/ee/core",
                                :xsi:schemaLocation  "http://www.mulesoft.org/schema/mule/core http://www.mulesoft.org/schema/mule/core/current/mule.xsd http://www.mulesoft.org/schema/mule/http http://www.mulesoft.org/schema/mule/http/current/mule-http.xsd http://www.mulesoft.org/schema/mule/file http://www.mulesoft.org/schema/mule/file/current/mule-file.xsd http://www.mulesoft.org/schema/mule/ee/core http://www.mulesoft.org/schema/mule/ee/core/current/mule-ee.xsd",
                                :xmlns:xsi           "http://www.w3.org/2001/XMLSchema-instance"}

                               ;; Config
                               (c/configuration-properties {:file "config.yaml"})
                               ;; Request config
                               (http/request-config* {:host "app-eapi-y1-shop-front.jp-e1.cloudhub.io" :type :https})
                               (c/flow
                                {:name "main-flow"}
                                (http/listener* {:config-ref "https_listener_config" :path "*"})
                                (c/logger* :INFO (dw/fx "attributes.requestPath"))
                                (http/request {:config-ref "https_request_config"
                                               :path (dw/fx "attributes.requestPath")}
                                              (http/query-params (dw/fx "attributes.queryParams"))))))
               
               {:writer wrt})
    
    (pp/pprint (list 'comment
                 (list 'mulify.utils/transpile
                   'mule-app
                   :g "pse"
                   :a project-name
                   :v "0.0.1"
                   :f default-main-xml))
      {:writer wrt})))

(defn- core-file [project-name]
  ;; (when (.exists (io/file f))
  ;;   (io/copy (io/file f) (io/file (str f ".bak")))
  ;;   (io/delete-file (io/file f)))
  (let [wrt (io/writer (io/file "src" project-name "core.clj") :append true)]
    (pp/pprint (list 'ns (symbol (str project-name ".core"))
                     (list ':gen-class)
                     (list ':require
                           '[clojure.tools.cli :refer (parse-opts)]
                           '[clojure.string :as str]
                           '[clojure.java.io :as io]))
               {:writer wrt})
    (pprint-document [:line] {:writer wrt})
    (main-functions wrt)))

(defn template [& [{opts :options [_ project template] :arguments}]]
  (if-not project
    (println "Please specify project name. See help with -h option.")
    (let [underscored-project (str/replace (or project "project") #"-" "_")]
      (pp/pprint (deps-file (or project "project") (or template :default)) {:writer (io/writer (io/file "deps.edn"))})
        (io/make-parents "src" underscored-project "core.clj")
        (condp = (keyword template)
          :web (web-core-file project)
          :mule (mule-core-file project)
          (core-file project)))))

(defn ancient [& [{opts :options [_ project template] :arguments}]]
  (letfn [(query-maven [group-id artifact-id]
            (->> {:query-params {:q (str "a:" artifact-id 
                                         ;; (when group-id
                                         ;;   (println "==>" group-id)
                                         ;;   (str "g:" group-id))
                                         )}}
                 
                 (silvur.http/get "http://search.maven.org/solrsearch/select")
                 deref
                 :body
                 util/json->edn
                 :response
                 :docs
                 (map :latestVersion)
                 ))
          (query-clojars [group-id artifact-id]
            (->> (silvur.http/get (str "https://clojars.org/api/artifacts/"
                                       (if group-id (str group-id "/") (str artifact-id "/"))
                                       artifact-id))
                 deref
                 :body
                 util/json->edn
                 :latest_version))]
    (let [deps (:deps (read-string (slurp "deps.edn")))
          max-version-string-length (apply max (map (comp count :mvn/version last) deps))]
      (doseq [[s v] deps :when (:mvn/version v)]
        (let [[artifact-id group-id] (reverse (str/split  (str s) #"/"))
              artifact (str artifact-id "-" (:mvn/version v) ".jar")]
          
          (println (format (str "%-20s%-50s" "%-" (+ 2 max-version-string-length) "s" "%-20s")
                           (or group-id artifact-id)
                           artifact-id
                           (:mvn/version v)
                           (->> (cons (query-clojars group-id artifact-id)
                                      (query-maven group-id artifact-id))
                                (keep identity)
                                (map (fn [x]
                                       (->> (str/split x  #"[.-]")
                                            (map #(try (util/str->int %)
                                                       (catch Exception e)))
                                            (keep identity)
                                            (cons x))))
                                (map #(take (count %) (rest (cycle %))))
                                (map vec)
                                (sort)
                                (reverse)
                                (first)
                                (last)))))))))


(defn deps [& [{opts :options [_ project template] :arguments}]]
  (println "Checking dependencies")
  (let [output (async/chan 1024)
        ;;done (atom #{})
        deps (filter (fn [[s v]] (:mvn/version v)) (:deps (read-string (slurp "deps.edn"))))]
    (doseq [[s v] deps]
      (let [[artifact-id group-id] (reverse (str/split  (str s) #"/"))
            artifact (str artifact-id "-" (:mvn/version v) ".jar")
            f (io/as-file (str/join "/" [".xc" "lib" artifact]))]
        (if (xc.file/exists? f)
          (do ;;(swap! done conj v)
            (async/>! output {:n s :result "Found"}))
          (async/go
            (loop [repos ["https://clojars.org/repo"
                          "https://repo1.maven.org/maven2"]]
              (if-not (seq repos)
                (do ;;(swap! done conj v)
                  (async/>! output {:n s :result "Not found"}))
                (let [repo (first repos)
                      
                      url (str/join "/"  [repo
                                          (if group-id
                                            (str/replace group-id #"\." "/")
                                            artifact-id)
                                          artifact-id
                                          (:mvn/version v)
                                          artifact])
                      {:keys [status body]} (silvur.http/get url)]
                  (if (= 200 status)
                    (let []
                      ;;(swap! done conj v)
                      (io/make-parents f)
                      (io/copy body f)
                      (async/>! output {:n s :result "Download"}))
                    (recur (rest repos))))))))))
    
    (loop [targets (set (keys deps))]
      (let [{:keys [n result]} (async/<! output)]
        (let [xs (disj targets n)]
          (println result n)
          (when (seq xs)
            (recur xs)))))))




